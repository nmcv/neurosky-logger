#!/usr/bin/env coffee


# Help/usage message
usageText = """

Broadcast data from serial port to network

Examples:

    0. List serial ports present in the system
        $ ./nslogger.coffee -a list

    1. Forward serial port /dev/tty0 to STDOUT
        $./nslogger.coffee -a forward /dev/tty0 -e

    2. Forward serial port /dev/tty1 to socket on default port (7777)
        $./nslogger.coffee -a forward /dev/tty1 -p

    3. Forward serial port /dev/tty1 to socket on port 3000 and echo to STDOUT
        $./nslogger.coffee -a forward /dev/tty1 -p 3000 -e

    3. Forward serial port /dev/tty1 to socket on port 3000 and echo to STDOUT
        $./nslogger.coffee -a forward /dev/tty1 -p 3000 -e


"""


#prompt = require "prompt"
cliff = require "cliff"
serialport = require "serialport"
log = require "npmlog"
args = require "optimist"
net = require "net"
redis = require "redis"
url = require "url"


# Register SIGINT firstly
process.on 'SIGINT', ->
    log.info "[i] (nslogger) Received (Ctrl-C). Shutting down.\n"
    process.exit(0)


printUsage = (args) ->
# Prints program usage and/or help
    args.showHelp()
    process.exit(0)


listSerialPorts = (cb) ->
# Lists system's serial ports

    serialport.list (err, ports) ->

        # Prepare header
        rows =
            [["ID", "Manufacturer", "Common name"]]

        # Cycle thru available ports
        ports.forEach (port, index) ->
            data =
            [
                index,
                port.manufacturer or '?',
                port.comName or '?'
            ]

            rows.push(data)

        # Output
        console.log "\n"
        console.log cliff.stringifyRows(rows, ['red', 'blue', 'green'])

        # Callback
        cb?()


clearScreen = ->
# Clears up the screen
    console.log `"\033[2J\033[0f"`


selectPort = (port, http_port, db, cb) ->
# Selects the serial port
# to work with and returns its
# object for further processing

    log.info "[i] Trying to get a hold of #{port}"

    opts =
        parser:
            serialport.parsers.readline("\n")
        baudrate:
            defaults.baud_rate
        # defaults for Arduino serial communication
        # dataBits:
        #     8
        # parity:
        #     'none'
        # stopBits:
        #     1
        # flowControl:
        #     false

    # Ready the serial port but open just yet
    sp = new serialport.SerialPort port, opts, false

    if args.argv.p or args.argv.port
        # Start web server
        startServer http_port

    if db
        initDatabase db[0], db[1], (dbClient) ->
            listenSerialEvents sp, dbClient
            log.info "[i] Connected to DB engine at #{db[0]}:#{db[1]}"
    else
        # Open port there and listen
        listenSerialEvents sp

    # Callback
    cb?(sp)


listenSerialEvents = (serialport, dbClient) ->
# Listens to data on a serial port
# Handles new data, close and error events

    # Register callbacks
    serialport.on "open", ->
        log.info "[+] Serial port OK"
        log.info "[+] Listening to serial data..."

        # Bind port closing event
        serialport.on "close", (err) ->
            log.info "[+] Serial port closed"

        # Bind new data event
        serialport.on "data", (data) ->
            processSerialPacket(data, dbClient or null)

    serialport.on "error", (err) ->
        log.error "[!] Serial port unreachable", err
        process.exit(1)

    # Finally open the port
    serialport.open()


processSerialPacket = (data, db_conn) ->
# Does something with data from the serial port
    if args.argv.e or args.argv.echo
        #process.stdout.write data
        console.log data

    # Save to DB
    if db_conn then dbSaveData(db_conn, data)

    # Finally, broadcast to network
    if args.argv.p or args.argv.port
        serverBroadcast(data)


startServer = (port, cb) ->
# Init the web server

    try
        server = net.createServer( (stream) ->
            serverReady stream, port)

        server.listen(port, ->
            log.info "[+] Started web server on #{port}")

    catch error
        log.error """[x] Couldn't start web server on port #{port}.
        Not interrupting the main flow. You may wish to restart the whole program."""

    # Callback
    cb?(server, port)


serverReady = (stream, port) ->
# Server per-connection handler

    # Identify this client
    stream.name = stream.remoteAddress + ":" + stream.remotePort

    # Track client
    clients.push stream

    # Send a nice welcome message
    stream.write "Welcome " + stream.name + "\n"

    log.info "[+] #{stream.name} joined"

    # Remove the client from the list when it leaves
    stream.on 'end', ->
        clients.splice(clients.indexOf(stream), 1)
        log.info "[+] #{stream.name} left \n"


serverBroadcast = (message, sender) ->
# Send a message to all web clients

    clients.forEach (client) ->
        # Don't send it to sender
        #if client is not sender

        client.write message + "\n"


initDatabase = (ip, port, cb) ->
# Initiates a database connection
# Returns a database object
    client = redis.createClient port, ip

    # On server connection error
    client.on "error", (err) ->
        log.error "[!] #{err}"

    # Callback
    cb?(client)

dbSaveData = (db, data, cb) ->
# Saves 'data' to database defined by 'db'
    client = db

    # Remove unwanted chars
    data = cleanUpDBData(data)

    # Save
    client.set Date.now(), data, redis.print

    # Callback
    cb?()


cleanUpDBData = (data) ->
# Removes unwanted bytes from 'data'
    clean = data.replace /(\r\n|\n|\r)?$/, ""


# Global defaults
defaults =
    http_port: 7777
    tty: "/dev/zero"
    baud_rate: 9600

# Keep track of web clients92
clients = []

# All about CLI args
args
    .usage(usageText)
    .alias("a", "action")
    .describe("a", "main program action")
    .demand("a")
    .alias("p", "port")
    .default("p", defaults.http_port)
    .describe("p", "socket port to forward to")
    .alias("e", "echo")
    .describe("e", "print serial data to STDOUT")
    .alias("j", "json")
    .describe("j", "pack serial data into JSON")
    .alias("d", "database")
    .describe("d", "mirror serial data to database")
    .alias("h", "help")
    .describe("h", "show usage help")
    .argv


# List ports
if args.argv.a is "list"
    listSerialPorts(->process.exit(0))


# Select port, start other services
# if desired, listen to s/p events
if args.argv.a is "forward"

    serial_port = args.argv._[0]
    if not serial_port
        printUsage args

    http_port =
        parseInt(args.argv.p) or
        parseInt(args.argv.port) or
        defaults.http_port

    connString = args.argv.d or args.argv.database
    db = connString.split(':') if connString

    selectPort serial_port, http_port, db or null


# Show usage/help
if args.argv.help or args.argv.h
    printUsage args