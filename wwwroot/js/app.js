var channels = [];
channels[0] = new Channel("Signal Quality", [0,0,0], "");
channels[1] = new Channel("Attention", [100,0,0], "");
channels[2] = new Channel("Meditation", [50,0,0], "");
channels[3] = new Channel("Delta", [219, 211, 42], "Dreamless Sleep");
channels[4] = new Channel("Theta", [245, 80, 71], "Drowsy");
channels[5] = new Channel("Low Alpha", [237, 0, 119], "Relaxed");
channels[6] = new Channel("High Alpha", [212, 0, 149], "Relaxed");
channels[7] = new Channel("Low Beta", [158, 18, 188], "Alert");
channels[8] = new Channel("High Beta", [116, 23, 190], "Alert");
channels[9] = new Channel("Low Gamma", [39, 25, 159], "Multi-sensory processing");
channels[10] = new Channel("High Gamma", [23, 26, 153], "???");

// Init Google's API in the first place
google.load("visualization", "1");

// Set up global AJAX opts
jQuery.ajaxSetup
(
    {
        url: "/data",
        dataType: "json",
        timeout: 1000
    }
);

// All globals should go here
var globals =
{
    // How often to update the graph by AJAXing
    // data in and post-processing it
    updateTimeout: 1000,

    // Set pointer to the first channel to be
    // processed (i.e. skip those before it)
    firstChannel: 0,

    // Max. vertical value (for scaling)
    verticalMaximum: null,

    // Channels that will not feed the graph
    skippedChannels: []
};


function Channel(label, color, desc, data)
// Channel class
{
    color = function(rgbArray)
    {
        return null
    }

    this.label = label;
    this.color = color(color);
    this.desc = desc;
    this.data = [];

    /*
        // Data template
        var data = [{
            date: new Date(),
            value: 0
        }]
    */
}


function getGraphData(graph, timeline)
// Fetch JSON with values for each channel
{
    $.ajax("/data").success(function(data)
    {
        // Remove the existing data
        //cleanChannelsUp(channels);


        // Current timestamp
        //
        // @todo: Add timestamp parsing from JSON file
        // to start counting seconds from the original
        // log file creation time.
        //
        var d = new Date();

        //console.log(data);
        $.each(data, function(index, val)
        {
            var channelDatum = val.split(',');

            // Drop if packet doesn't contain enough values
            if (channelDatum.length != channels.length)
            {
                console.warn("Dropping malformed packet (index: %d)", index);
                $.continue;
            }

            if (val && !val.match(/error/i))
            {
                // Append value to each single channel
                for (i = globals.firstChannel; i < channels.length; i++)
                {
                    channels[i].data.push(
                        {date: new Date(d), value: "" + channelDatum[i]});

                    // Set new vertical maximum
                    (channelDatum[i] > globals.verticalMaximum)
                        && (globals.verticalMaximum =
                                parseInt(channelDatum[i], 10))
                }
            }
            else
            {
                //console.warn("Line %d didn't contain any data", index + 1);
            }

            // Set the time to "next second"
            d.setSeconds(d.getSeconds() + 1);

            // Tell the graph to expand its range
            graph.end = d;

        });

        drawVisualization(graph);

        //@dbg
        globals.timeline = timeline;
    });
}


$(document).ready(function()
{
    init();
    listenUIEvents();
    fixUI();
});


function init()
// Main init code of the page
{
    // Create and populate a data table.
    var datatable = new google.visualization.DataTable();
    datatable.addColumn('datetime', 'time');

    for (i = globals.firstChannel; i < channels.length; i++)
    {
        datatable.addColumn('number', channels[i]["label"]);
    }

    // Instantiate our graph object.
    globals.graph = new links.Graph( $('#graph').get(0) );

    //graph.setAutoScale(false);

    // Instantiate our timeline object
    globals.timeline = new links.Timeline( $('#timeline').get(0) );

    // Go
    drawGraph();
}

function drawGraphWithInterval()
// Draw graph with interval
{
    globals.updateInterval = setInterval(function()
    {
        drawGraph();
    }, globals.updateTimeout);
}


function drawGraph()
// Draw graph once
{
    getGraphData(globals.graph, globals.timeline);
}


function cleanChannelsUp(channels)
// Purge data from all channels
{
    for (i = globals.firstChannel; i < channels.length; i++)
    {
        channels[i].data = [];
    }
}


function isSkippedChannel(channelNo)
// Return true if the channelNo is one
// of the skipped_channels
{
    var channelNo = parseInt(channelNo, 10);
    var result = false;

    for (i = globals.firstChannel; i < globals.skippedChannels.length; i++)
    {
        if (i == channelNo)
        {
            result = true;
            break;
        }
    }

    return result;
}


function drawVisualization(graphObject)
// Draw the main "Timeline" graph
{
    var datasets = [];

    for (z = globals.firstChannel; z < channels.length; z++)
    {
        //channels[z].data.push(channelDatum[z])

        datasets.push(
            {
                label: channels[z].label,
                data: channels[z].data
            }
        );

        // Clear up the channel to avoid
        // duplicate graphing
        channels[z].data = [];
    }

    // Specify main graph options
    var graph_options =
    {
        "width":  "100%",
        "height": "400px",
        "vMax": 1.05 * globals.verticalMaximum,
        "line":
                {
                    "width": 0.5  // width applied to all lines
                },
        "legend": {toggleVisibility: true}
    };

    // Draw our graph with the created data and options
    graphObject.draw(datasets, graph_options);
}


function drawTimelineVisualization(timelineObject)
// Draw the "Events" graph
{
    var timeline_data = [];

    // Draw just one channel for testing purposes
    for (i = globals.firstChannel; i < channels[4].data.length; i++)
    {
        var d = new Date();

        var style = 'height:' + Math.round(channels[4].data[i].value/1000) + 'px;';
        var requirement = '<div class="raw-value" style="' + style + '" ' +
                                'title="Raw value"></div>';

        d.setSeconds((channels[4].data[i].date.getSeconds() + 1));

        var item = {
            'group': channels[4].label,
            'start': channels[4].data[i].date,
            'end': d,
            'content': requirement
        };

        timeline_data.push(item);
    }

    // Specify timeline options
    var timeline_options =
    {
       "width":  "100%",
       "height": "300px",
       "style": "box"
    };

    // Draw our graph with the created data and options
    timelineObject.draw(timeline_data, timeline_options);
}


function fixUI()
// Manuipulate DOM after init
{
    $('.graph-frame').addClass('panel');
}


function listenUIEvents()
{
    $('#update-trigger').on('click', function(e)
    {
        console.log("[i] Update trigger clicked");

        $(this).hasClass('active')
        ?
        clearInterval(globals.updateInterval)
        :
        drawGraphWithInterval()
    });
}