#!/usr/bin/env coffee





# Help/usage message
usageText = """

Viewer for data logged by nslogger


Usage:

    ./nsviewer.coffee [options]


Examples:

    0. Will try to start the viwer service on TCP port 8080
    $ ./nsviewer.coffee --start -p 8080

    1. Start the server on default port (8888) and read from STDIN
    $ echo "test data" | ./nsviewer.coffee --start -s

    2. Start the server on default port (8888), listen from standard input and convert data to JSON for the client side
    $ echo "test data" | ./nsviewer.coffee --start -s -j


Note:

    This is not a daemon. Killable with regular (Ctrl-C).


"""

#prompt = require "prompt"
#cliff = require "cliff"
log = require "npmlog"
args = require "optimist"
redis = require "redis"
express = require "express"
buffer = require "buffer"
fs = require "fs"

# Create a new level for logging
log.addLevel(
    "debug"
    0
    {
        fg: "#ffffff"
        bg: "#00ff00"
    }
)

# Register SIGINT firstly
process.on 'SIGINT', ->
    log.info "[i] (nsviewer) Received (Ctrl-C). Shutting down.\n"
    process.exit(0)


printUsage = (args) ->
# Prints program usage and/or help
    args.showHelp()
    process.exit(0)


prepareRawData = (rawData) ->
# Looks for JSON option from CLI and returns
# the same with respect to that. Put out as a se-
# parate procedure as I see parsing options grow
# in the future
    rawData = rawData.split globals.lineSeparator

    if args.argv.j or args.argv.json
        JSON.stringify rawData
    else
        rawData

# All about CLI args
args
    .usage(usageText)
    .describe("start", "start the server")
    .describe("root", "web root folder")
    .describe("f", "read from file (incompatible with -s)")
    .alias("f", "file")
    .describe("s", "read from STDIN (incompatible with -f)")
    .alias("s", "stdin")
    .alias("p", "port")
    .describe("p", "HTTP port to run on")
    .alias("j", "json")
    .describe("j", "JSONify web streams like STDIN")
    .alias("d", "delimiter")
    .describe("d", "line separator (\\r, \\r\\n or \\n)")
    .alias("h", "help")
    .describe("h", "show usage help")
    .argv
    # .describe("stop", "stop the server")
    # .describe("restart", "restart the server")


# Globals
globals =
    httpPort: parseInt(args.argv.port, 10) or parseInt(args.argv.p, 10) or 8888
    wwwRoot: args.argv.root or "wwwroot/"
    stdinData: ""
    fileData: ""
    lineSeparator: args.argv.delimiter or args.argv.d or '\r\n'
    userFile: args.argv.f or args.argv.file


# Logging is helpful (as well as weird comments)
log.level = "verbose" # change to "info" or "error" upon release
log.verbose "[d] Logging set to \"verbose\"" # Mic test
log.verbose "[d] Line separator is set to #{JSON.stringify(globals.lineSeparator)}"


# STDIN or from file?
if (args.argv.f or args.argv.file) and (args.argv.s or args.argv.stdin)
    log.error "Option -f is incompatible with -s\n"
    printUsage args


# From file
if globals.userFile
    fs.readFile globals.userFile, "utf8", (err, data) ->
        if err
            log.error err
            process.exit(1)
        else
            globals.fileData = data #.replace(/^(\r\n|\r|\n)$/, "")


# Anything from STDIN?
if args.argv.s or args.argv.stdin
    process.stdin.resume()
    process.stdin.setEncoding "utf8"

    process.stdin.on "data", (chunk) ->
        globals.stdinData += chunk

    process.stdin.on "end", ->
        byteCount = Buffer.byteLength(globals.stdinData, "utf8")
        log.verbose "[+] Fetched #{byteCount} bytes of data from STDIN"


# On received command to start the web server
if args.argv.start

    app = express()

    # Needless due to configuration below
    # to serve everything from globals.wwwRoot as
    # static content
    #
    # app.get "/", (req, res) ->
    #     res.redirect "#{globals.wwwRoot}index.html"

    # Serves what was on stdin
    app.get "/data", (req, res) ->
        if globals.stdinData
            # res.send JSON.stringify(
            #     globals.stdinData.split globals.lineSeparator
            # )
            res.send prepareRawData(globals.stdinData)
            log.verbose "[i] Served STDIN data over HTTP"

        else if globals.fileData
            res.send prepareRawData(globals.fileData)
            log.verbose "[i] Served file contents over HTTP"

        else
            res.end()
            #log.verbose "[i] Had no STDIN data to serve over HTTP"


    # Configure the server before running it
    app.configure ->
        #app.use express.methodOverride()
        #app.use express.bodyParser()
        app.use express.static(__dirname + '/' + globals.wwwRoot)

        app.use express.errorHandler
        {
            dumpExceptions: true
            showStack: true
        }

        app.use(app.router);


    # Go
    app.listen globals.httpPort, ->
        log.info "[i] Listening on port #{globals.httpPort}"


# Show usage/help
if args.argv.help or args.argv.h or 'help' in args.argv._
    printUsage args

#log.info "[i] All done. Quit."
#printUsage args
